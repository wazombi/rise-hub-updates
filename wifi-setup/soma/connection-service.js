var util = require('util');
var bleno = require('../node_modules/bleno');

var ConnectionStatusCharacteristic = require('./connection-status-characteristic');
var availableNetworksCharacteristic = require('./available-networks-characteristic');
var wlan0AddrCharacteristic = require('./wlan-addr-characteristic');
var selectNetworkCharacteristic = require('./select-network-characteristic');

function ConnectionService(ConnectionManager) {
	bleno.PrimaryService.call(this, {
		uuid: '22222222222222222222222222222222',
		characteristics: [
			new ConnectionStatusCharacteristic.ConnectionStatusCharacteristic(ConnectionManager),
			new availableNetworksCharacteristic.AvailableNetworksCharacteristic(ConnectionManager),
			new wlan0AddrCharacteristic.Wlan0AddrCharacteristic(ConnectionManager),
			new selectNetworkCharacteristic.SelectNetworkCharacteristic(ConnectionManager)
			]
		});
	}

util.inherits(ConnectionService, bleno.PrimaryService);

module.exports.ConnectionService = ConnectionService;


