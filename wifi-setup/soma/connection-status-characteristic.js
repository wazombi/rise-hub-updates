var util = require('util');
var bleno = require('../node_modules/bleno');
var connectionManager = require('./connection-manager');

function ConnectionStatusCharacteristic(ConnectionManager){
	bleno.Characteristic.call(this, {
		uuid: '11111111111111111111111111111111',
		properties: ['read','notify'],
		descriptors: [ new bleno.Descriptor({
			uuid: '2901',
			value: 'Gets the status (OK, ERROR)'
		})]
	});
    
	this.cm = ConnectionManager;

	this.updateValueCallback = null;
}

util.inherits(ConnectionStatusCharacteristic, bleno.Characteristic);

ConnectionStatusCharacteristic.prototype.notify = function (value) {
	
	console.log("Notification sent " + value);
	this.updateValueCallback(Buffer.alloc(1,value));
}

ConnectionStatusCharacteristic.prototype.onReadRequest = function(offset, callback){

	console.log("Connection Status Read Request");
	
	var ssid = this.cm.stat.ssid;

	if(ssid === null) { ssid = "";}

	if(ssid.length > 19) {
		ssid = ssid.substring(0,20);
	}

	var data = new Buffer(ssid.length + 1);

	if(ssid.length > 0) {
		//TODO: Support SSID's longer than one payload
		data.write(ssid, 1);
	}

	data.writeUInt8(this.cm.statCode, 0);

	if(data.length > 20) {
		console.log("Error: Support SSID's longer than one payload");
		callback(this.RESULT_UNLIKELY_ERROR, null);
	}
	else {
		callback(this.RESULT_SUCCESS, data);
	}
};

ConnectionStatusCharacteristic.prototype.onSubscribe = function (maxValueSize, updateValueCallback) {
	console.log("Connection Status Subscribe");
	this.updateValueCallback = updateValueCallback;
	this.cm.setStatusNotifier(this);
}

ConnectionStatusCharacteristic.prototype.onUnsubscribe = function() {
	console.log("Connection Status Unsubscribe");
	this.updateValueCallback = null;
	this.cm.clearStatusNotifier();
}

module.exports.ConnectionStatusCharacteristic = ConnectionStatusCharacteristic;
