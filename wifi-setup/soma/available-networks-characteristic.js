var util = require('util');
var bleno = require('../node_modules/bleno');
var connectionManager = require('./connection-manager');


var BUILD_NETWORK_LIST_CMD = 0xFF;


function AvailableNetworksCharacteristic(ConnectionManager){
	bleno.Characteristic.call(this, {
		uuid: '11111111111111111111111111111112',
		properties: ['write', 'notify'],
		descriptors: [ new bleno.Descriptor({
			uuid: '2901',
			value: 'Gets the list of networks'
		})]
	});
    
	this.cm = ConnectionManager;
	this._updateValueCallback = null;
}

util.inherits(AvailableNetworksCharacteristic, bleno.Characteristic);

//PUBLIC

AvailableNetworksCharacteristic.prototype.onWriteRequest = function (data, offset, withoutResponse, callback) {
	console.log("Available Networks Write Request");
	
	if(offset) {
		callback(this.RESULT_ATTR_NOT_LONG);
	}
	else {
		
		if(data.length === 1 && data[0] === BUILD_NETWORK_LIST_CMD) {
			this.cm.getNetworkList(this);
			callback(this.RESULT_SUCCESS);
		}
		else {
			console.log("Invalid request");
			callback(this.RESULT_UNLIKELY_ERROR);
		}
	}
};

AvailableNetworksCharacteristic.prototype.notify = function(buf) {

	console.log("AvailableNetworksNotify");
	try {
		this._updateValueCallback(buf);
	}
	catch (err) {
		console.log(err);
		console.log("Error: Subscribe to notifications first!");
	}
}

AvailableNetworksCharacteristic.prototype.onSubscribe = function(maxValueSize, updateValueCallback) {
	console.log("Available Networks onSubscribe");
	this._updateValueCallback = updateValueCallback;
	this.cm.setNetworksNotifier(this);
}


AvailableNetworksCharacteristic.prototype.onUnsubscribe = function() {
	console.log("Available Networks onUnsubscribe");
	this._updateValueCallback = null;
	this.cm.clearNetworksNotifier();
}


module.exports.AvailableNetworksCharacteristic = AvailableNetworksCharacteristic;


