
var util = require('util');
var bleno = require('../node_modules/bleno');
var connectionManager = require('./connection-manager');

function SelectNetworkCharacteristic(ConnectionManager){
	bleno.Characteristic.call(this, {
		uuid: '11111111111111111111111111111114',
		properties: ['write'],
		descriptors: [ new bleno.Descriptor({
			uuid: '2901',
			value: 'Selects the desired network'
		})]
	});
    
	this.cm = ConnectionManager;

	this.bytesExpected = null;
	this.passwordBuffer = "";
	this.SSIDBuffer = null;
}

util.inherits(SelectNetworkCharacteristic, bleno.Characteristic);

//SHOULD BE PRIVATE
SelectNetworkCharacteristic.prototype.resetBuffers = function() {
	this.bytesExpected = null;
	this.passwordBuffer = "";
	this.SSIDBuffer = null;
}

SelectNetworkCharacteristic.prototype.bufferSSIDAndPassword = function(ssid, password) {
	if(ssid !== null && this.SSIDBuffer === null) { 
		this.SSIDBuffer = ssid;
	}

	this.bytesExpected -= password.length;
	this.passwordBuffer += password;

	console.log("Buffered SSID index: " + this.SSIDBuffer);
	console.log("Buffered password: " + this.passwordBuffer); 
	
	//Check if all the bytes have been received
	if(this.bytesExpected === 0) {
		console.log("Entire password received");
		this.cm.selectNetwork(this.SSIDBuffer, this.passwordBuffer);
		this.resetBuffers();
	}
	else if (this.bytesExpected < 0) {
		console.log("Received more bytes than initially promised");
		console.log("Resetting Buffers");
		this.resetBuffers();	
	}
	else {
		console.log("Some bytes still missing");
	}
}

//PUBLIC
SelectNetworkCharacteristic.prototype.onWriteRequest = function (data, offset, withoutResponse, callback) {
	console.log("Select Network Write Request");
	
	if(offset) {
		callback(this.RESULT_ATTR_NOT_LONG);
	}
	else {
		//First writeRequest, nothing received yet
		if(this.bytesExpected === null) {
			console.log("bytesExpected === null");
			//Check if data long enough (SSID index and password length must be present
			//in the first packet
			if(data.length < 2) {
				console.log("Data too short");
				callback(this.RESULT_INVALID_ATTRIBUTE_LENGTH);			
			}
			else { //Enough data available
				//Get the index of selected SSID and password length
				var ssid = data[0];
				this.bytesExpected = data[1];
				var password = data.toString('utf8',2);
				
				//Buffer the received data
				this.bufferSSIDAndPassword(ssid, password);
			}
		}
		else { //Buffer the rest of the password
			this.bufferSSIDAndPassword(null, data.toString('utf8'));
		}
		callback(this.RESULT_SUCCESS);
	}
};			

module.exports.SelectNetworkCharacteristic = SelectNetworkCharacteristic;
