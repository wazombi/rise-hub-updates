
var execSync = require('child_process').execSync;



module.exports = {
	connect: connect,
	get_status: get_status,
	save_config: save_config
}

var network_id = 0;
var interface_name = "wlan0";

function result_success(result) {
	
	result = result.split("\n");

//	if(!interface_is(interface_name, result[0])) {
//		console.log("Error: Wrong interface selected");
//		return false;
//	}

	if(result[0] != "OK") {
		console.log("Error: Command result not OK");
		return false;
	}

	return true;
}

function interface_is(target, test) {
	// "Selected interface 'wlan0'"
	var interf = test.split(" ")[2];
	interf = interf.substring(1, interf.length - 1);

	if (interf === target) {
		return true;
	}
	else {
		console.log("interface false");
		return false;
	}
}

function connect(ssid, psk) {
	
	//Disable the current network
	if(disable_network(network_id)) {
		console.log("Disabled network " + network_id);
	}
	else {
		console.log("Unable to connect: can't disable network " + network_id);
		return false;
	}
	
	//Set the SSID	
	if(set_ssid(network_id, ssid)) {
		console.log("Successfully set SSID to " + ssid + " for network " + network_id);
	}
	else {
		console.log("Unable to connect: can't set ssid");
		return false;
	}
	
	//Set the PSK

	if(set_psk(network_id, psk)) {
		console.log("Successfully set the PSK to " + psk + " for network " + network_id);
	}
	else {
		console.log("Unable to connect: can't set the PSK");
		return false;
	}

	if(enable_network(network_id)) {
		console.log("Successfully enabled network " + network_id);
	}
	else {
		console.log("Unable to connect: can't enable the network");
		return false;
	}

	return true;
}

function disable_network(network_id) {
	
	var DISABLE_NETWORK_CMD = "wpa_cli -i 'wlan0' disable_network " + network_id;

	var result = execSync(DISABLE_NETWORK_CMD, {encoding: "utf-8"});

	if(!result_success(result)) {
		console.log("Error: wpa_cli disable_network failed");
		return false;
	}

	return true;
}

function set_ssid(id, ssid) {
	
	//wpa_cli is buggy, ssid and psk must be in quotes and additionally be wrapped in
	//single quotes to work
	var SET_SSID_CMD = "wpa_cli -i 'wlan0' set_network " + id + " ssid \'\"" + ssid + "\"\'"; 
 
	var result = execSync(SET_SSID_CMD, {encoding: "utf-8"});
	
	if(!result_success(result)) {
		console.log("Error: wpa_cli set_network ssid failed");
		return false;
	}

	return true;
}

function set_psk(id, psk) {
	
	//wpa_cli is buggy, ssid and psk must be in quotes and additionally be wrapped in
	//single quotes to work
	var SET_PSK_CMD = "wpa_cli -i 'wlan0' set_network " + id + " psk \'\"" + psk + "\"\'";

	var result = execSync(SET_PSK_CMD, {encoding: "utf-8"});

	if(!result_success(result)) {
		console.log("Error: wpa_cli set_network psk failed");
		return false;
	}
	
	return true;
}


function enable_network(id) {
	
	var ENABLE_NETWORK_CMD = "wpa_cli -i 'wlan0' enable_network " + id;

	var result = execSync(ENABLE_NETWORK_CMD, {encoding: "utf-8"});

	if(!result_success(result)) {
		console.log("Error: wpa_cli enable_network failed");
		return false;
	}

	
	return true;
}

function save_config() {
	
	var SAVE_CONFIG_CMD = "wpa_cli -i 'wlan0' save_config";

	var result = execSync(SAVE_CONFIG_CMD, {encoding: "utf-8"});
	
	if(!result_success(result)) {
		console.log("Error: wpa_cli save_config failed)");
		return false;
	}
	console.log("Wifi Configuration successfully saved");
	return true;
}

function parse_status_result(result) {

	var stat = {
		bssid: null,
		ssid: null,
		id: null,
		key_mgmt: null,
		state: null,
		ip: null,		
	}

	for(i = 0; i < result.length; i++) {
		var parameter = result[i].split("=");
		
		if(parameter[0] === "bssid") {
			stat.bssid = parameter[1];
		}
		else if(parameter[0] === "ssid") {
			stat.ssid = parameter[1];
		}
		else if(parameter[0] === "id") {
			stat.id = parameter[1];
		}
		else if(parameter[0] === "key_mgmt") {
			stat.key_mgmt = parameter[1];
		}
		else if(parameter[0] === "wpa_state") {
			stat.state = parameter[1];
		}
		else if(parameter[0] === "ip_address") {
			stat.ip = parameter[1];
		}
		else {}
	}

	return stat;

	}

function get_status() {

	var GET_STATUS_CMD = "wpa_cli -i 'wlan0' status";
	
	var result = execSync(GET_STATUS_CMD, {encoding: "utf-8"});

	result = result.split("\n");

	//if(!interface_is(interface_name, result[0])) {
	//	console.log("Error: wpa_cli status wrong interface");
	//	return stat;
	//}
	
	var stat = parse_status_result(result);
	console.log(stat);
	return stat;
}



