// Code modified from the bleno "PizzaSquat" example to
// suit the needs of SOMA Connect. This application sends 
// using BLE a list of available wifi networks to the SOMA
// SmartShades App. User can then select the network and
// send the password to this application. This application
// can be integrated to the SOMA Connect Hub. 


var util = require('util');

//
// Require bleno peripheral library.
// https://github.com/sandeepmistry/bleno
//
var bleno = require('../node_modules/bleno');

// Handles and keeps track of all connections
var ConnectionManager = require('./connection-manager');


//
// Name of our BLE Service
//
var name = 'SOMAConnectService';

var ConnectionService = require('./connection-service');
var manager = new ConnectionManager.ConnectionManager();
manager.getNetworkList();
var connectionService = new ConnectionService.ConnectionService(manager);

// Wait until the BLE radio powers on before attempting to advertise.
// If you don't have a BLE radio, then it will never power on!
//
bleno.on('stateChange', function(state) {
  if (state === 'poweredOn') {
    //
    // We will also advertise the service ID in the advertising packet,
    // so it's easier to find.
    //
    bleno.startAdvertising(name, [connectionService.uuid], function(err) {
      if (err) {
        console.log(err);
      }
    });
  }
  else {
    bleno.stopAdvertising();
  }
});

bleno.on('advertisingStart', function(err) {
  if (!err) {
    console.log('advertising...');
    //
    // Once we are advertising, it's time to set up our services,
    // along with our characteristics.
    //
    bleno.setServices([
      connectionService
    ]);
  }
});
