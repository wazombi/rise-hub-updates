
var exec = require('child_process').exec;
var wpa_cli = require('./wpa_cli');

var ConnectionStatus = {
	"ERROR": 0,
	"DISCONNECTED": 1,
	"INACTIVE": 2,
	"SCANNING": 3,
	"ASSOCIATING": 4,
	"ASSOCIATED": 5,
	"COMPLETED": 6,
	"CONNECTED_WITH_IP": 7
};

const STATUS_CHECK_INTERVAL = 5000;

const CONNECTION_TIMEOUT = 20000; //ms

// This is the main class/object that holds the connection data
// quieries for available networks should be done elsewhere


function ConnectionManager() {
	
	this.statNotifier = null;
	this.statCheckIntervalID = null;
	this.statCode = null;
	this.stat = null;

	this.networksNotifier = null;
	this.availableNetworks = {};
	this.noOfAvailableNetworks = 0;
	this.availableNetworksBuffer = null;

	this.selectedSSID = null;
	this.selectedSSIDPassword = null;
	//Designates the index in the list that will be sent to the mobile app on next 
	//request
	this.listIndex = 0;
	
	this.macAddr = "";

	//Set the MAC
	this.getWlan0MAC();
	this.checkStatus();
};

ConnectionManager.prototype.setNetworksNotifier = function(notifier) {
	this.networksNotifier = notifier;
}

ConnectionManager.prototype.clearNetworksNotifier = function() {
	this.networksNotifier = null;
}

ConnectionManager.prototype.setStatusNotifier = function(notifier){ 
	this.statNotifier = notifier;
	this.statCheckIntervalID = setInterval(this.checkStatus.bind(this), STATUS_CHECK_INTERVAL);
}

ConnectionManager.prototype.clearStatusNotifier = function() {
	clearInterval(this.statCheckIntervalID);
	this.statNotifier = null;
	this.statCheckIntervalID = null;
}

ConnectionManager.prototype.checkStatus = function() {
	console.log("Status Check");
	
	var stat = wpa_cli.get_status();
	
	if(this.compareStatus(stat)) {
		this.changeStatus(stat);
	}
}

ConnectionManager.prototype.changeStatus = function (stat) {
	console.log("Status Change");
	
	this.setStatus(stat);

	try {
		this.statNotifier.notify(this.statCode);
	}
	catch (err) {
		console.log("Error: Notifier not available");
	}
} 

ConnectionManager.prototype.setStatus = function(stat) {
	console.log("Set Status");

	this.stat = stat;

	//Figure out the status code...
	if(this.stat.state === "COMPLETED" && this.stat.ip != null) {
		this.statCode = ConnectionStatus["CONNECTED_WITH_IP"];
	}
	else if(this.stat.state in ConnectionStatus) {
		this.statCode = ConnectionStatus[this.stat.state];
	}
	else {
		this.statCode = ConnectionStatus["ERROR"];
	}
}

//Returns true if status has changed compared to this.stat
ConnectionManager.prototype.compareStatus = function(stat) {
	
	if(this.stat === null && stat != null ||
	   this.stat.bssid != stat.bssid ||
	   this.stat.ssid != stat.ssid ||
	   this.stat.id != stat.id ||
	   this.stat.key_mgmt != stat.key_mgmt ||
	   this.stat.state != stat.state ||
	   this.stat.ip != stat.ip) {
		
		return true;
	}

	return false;
}


ConnectionManager.prototype.getWlan0MAC = function() {
	var that = this;
	
	const getMACCmd = "cat /sys/class/net/wlan0/address";
	var getMAC = exec(getMACCmd, function (error, stdout, stderr) {
		that.macAddr = stdout.substring(0, stdout.length - 1);
	});	
}

//Get the list of available networks
ConnectionManager.prototype.getNetworkList = function(availableNetworksCharacteristic) {

	//To call buildNetworkList() from the exec callback	
	var that = this;

	//TODO: Get rid of cached results
	var cmd = "sudo iwlist scan | grep -o \"ESSID:.*\" | cut -c7-" ;
	var child = exec(cmd, function(error, stdout, stderr) {
		
		var ssid_list = [];
		
		if(error) {
			console.log(error);
		}

		var tmp_list = stdout.split("\n");
		var uniques = [];
		for(i = 0; i < tmp_list.length; i++) {
			
			//Strip additional quote marks from the network names
			var ssid = tmp_list[i].substring(1, tmp_list[i].length - 1);

			if(!(ssid in uniques)) {
				ssid_list.push(ssid);
				uniques.push(ssid);
			}
		}

		that.buildNetworkList(ssid_list);
		//Send the networklist using notifications
		that.sendNetworkList();
	});

	child.on('error', (err) => {
		console.log("Child process encountered an error: " + err);
	});
}


ConnectionManager.prototype.buildNetworkList = function (array) {
	console.log("buildNetworkList");

	//Reset the lists
	this.availableNetworks = {};	
	this.availableNetworksBuffer = null;
	this.listIndex = 0;
	
	if(array.length === 0) {
		console.log("No networks found ...");
	}
	else {
		console.log(array.length + " networks found:");	
	
		this.noOfAvailableNetworks = array.length;
		
		for(i = 0; i < this.noOfAvailableNetworks; i++) {
			//Map the networks to indexes
			this.availableNetworks[i] = array[i];
			var ssidLength = array[i].length;
			var buf = new Buffer(2 + ssidLength);
			buf[0] = i;
			buf[1] = ssidLength;
			buf.write(array[i], 2);

			//First element of the final buffer will be the total number of networks
			if(this.availableNetworksBuffer === null) {
				this.availableNetworksBuffer = Buffer.alloc(1, this.noOfAvailableNetworks);
			}

			
			var newBufLen = this.availableNetworksBuffer.length + 
							buf.length;
			this.availableNetworksBuffer = Buffer.concat([
					this.availableNetworksBuffer,
					buf
			], newBufLen);
			
		}
		console.log("Build a list of networks: ");
		console.log(this.availableNetworks);
		console.log(this.availableNetworksBuffer);
	}
}

ConnectionManager.prototype.sendNetworkList = function () {
	console.log("sendNetworkList");	
	
	for(var i = 0; i < this.noOfAvailableNetworks; i++) {
		
		var ssid = this.availableNetworks[i];

		if(ssid.length > 19) {
			var ssid = ssid.substring(0, 19) + ".";
		}
		
		var payload = Buffer.alloc(ssid.length + 1);
		payload[0] = i;
		payload.write(ssid, 1);
		try {
			this.networksNotifier.notify(payload);
		}
		catch (err) {
			console.log("ERROR: Subscribe to Available Networks Notifications first");
		}
	}
}

ConnectionManager.prototype.connectToSSID = function () {
	
	if(wpa_cli.connect(this.selectedSSID, this.selectedSSIDPassword)) {
		
		var that = this;

		setTimeout(function () { 
			
			that.checkStatus();
			if(that.statCode > ConnectionStatus["ASSOCIATED"])	{
				console.log("Successfully connected to the desired network");
				wpa_cli.save_config();
			}
			else {
				console.log("Connecting not finished or IP not received");
			}
		}, CONNECTION_TIMEOUT);
	}
}

ConnectionManager.prototype.isSSIDValid = function(ssid) {
	
	if((ssid > this.noOfAvailableNetworks - 1) || (ssid < 0)) {
		console.log("Invalid SSID selected");
		return false;	
	}
	return true;
}


ConnectionManager.prototype.selectNetwork = function(ssid, password) {

	if(this.isSSIDValid(ssid)){
		console.log("SSID and Password seem valid");
	
		this.selectedSSID = this.availableNetworks[ssid];
		this.selectedSSIDPassword = password;

		console.log("All available networks: ");
		console.log(this.availableNetworks);
		console.log("Selected SSID: #" + ssid + " " + this.selectedSSID);
		console.log("Password of selected SSID: " + this.selectedSSIDPassword);

		this.connectToSSID();

	}
	else { 
		console.log("Selected SSID not valid!");
	}
}

module.exports.ConnectionManager = ConnectionManager;
