
var util = require('util');
var bleno = require('../node_modules/bleno');
var connectionManager = require('./connection-manager');

function Wlan0AddrCharacteristic(ConnectionManager){
	bleno.Characteristic.call(this, {
		uuid: '11111111111111111111111111111113',
		properties: ['read'],
		descriptors: [ new bleno.Descriptor({
			uuid: '2901',
			value: 'Gets the MAC of wlan0'
		})]
	});
    
	this.cm = ConnectionManager;
}

util.inherits(Wlan0AddrCharacteristic, bleno.Characteristic);

Wlan0AddrCharacteristic.prototype.onReadRequest = function(offset, callback){

	console.log("WLAN0 Address Read Request");
	var payload = Buffer.from(this.cm.macAddr);
	
	callback(this.RESULT_SUCCESS, payload);
}

module.exports.Wlan0AddrCharacteristic = Wlan0AddrCharacteristic;
