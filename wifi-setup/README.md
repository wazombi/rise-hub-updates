# README #

### What is this repository for? ###

* This repository contains code to make the Raspberry Pi act as a BLE peripheral and communicate with a mobile app (acting as central). Raspberry Pi sends a list of available wifi networks to the mobile app. The user can then send back the selected SSID along with a password.  
* This is used in the SOMA Connect device to allow easy configuration of wifi networks using the SmartShades app. 
* The code is not meant to be used as a standalone application, it should be integrated to the Soma Connect. 

### How do I get set up? ###

* Make sure you have Node JS installed. It would be best to use the actual Soma Connect image, that is already preconfigured or configure one yourself based on the tutorial in 
"Repository for SOMA Connect"
* Clone the repository to ~/ble_peripheral/ 
* Make sure bleno (https://github.com/sandeepmistry/bleno) is installed (should come with the repo, not sure)
* Navigate to ~/ble_peripheral/node_modules/bleno/examples/soma/
* To run the example and communicate to it (using nRF Connect App, for example) run sudo node peripheral.js
* To integrate it to the Soma Connect application you need the following files: 
    * peripheral.js - app entry to the example, take the basic usage of the example here.
    * connection-manager.js - class to hold the connection status variables, as well as available networks, selected SSID and password etc
    * connection-service.js - creates the BLE Service
    * available-networks-characteristic.js - read requests to get the list of available networks, write request to send back the selected network and its password
    * connection-status-characteristic.js - read request to get the connection status


### Characteristics ###

#### available-networks-characteristic.js ####

* uuid: 11111111111111111111111111111112

* This characteristic controls wifi network scanning and allows the central (mobile app) to read the full list of networks. 

    * Use the write request to send 0xFF - the application on the raspberry starts scanning for wifi networks and sends a notification with 0xFF when scan complete. Using the read request you can now get the list of all available networks. 
    * Example: 
 
    * arrayOfNetworks = ["WIFI1", "WIFI2", "MYWIFI"];
    * ConnectionManager assigns indexes to all of the networks and creates a dictionary = { "0" : "WIFI1", "1" : "WIFI2", "2": "MYWIFI"}
    * To send the list to the client over BLE (Read request to available-networks-characteristic) the ConnectionManager builds a buffer that contains all the SSID's along with their index and length. the SSID's are packed back-to-back in the buffer and separated using the "length" byte. First byte of the list is the number of networks in the buffer. The overall structure of the list is: 
        * NO_OF_NETWORKS INDEX_OF_FIRST_SSID LENGTH_OF_FIRST_SSID SSID INDEX_OF_SECOND_SSID LENGTH_OF_SECOND_SSID SECOND_SSID ... 
    * Using the current example, the buffer created would be: 
        * networkList = [0x03, 0x00, 0x05, 0x57, 0x49, 0x46, 0x49, 0x31, 0x01, 0x05, 0x57, 0x49, 0x46, 0x49, 0x32, 0x02, 0x06, 0x4D, 0x59, 0x57, 0x49, 0x46, 0x49]
    * Lists that are longer than 20 bytes (BLE limitation) are sent using multiple read requests, the ConnectionManager keeps track of how many bytes are sent and which bytes to send next. When the full list is sent the ConnectionManager loops back to the beginning.

#### connection-status-characteristic.js ####

*/ TODO /* SOME FUNCTIONALITY STILL MISSING - SENDS ONE REPLY EVERYTIME, DOESN'T USE ACTUAL STATUS

* uuid: '11111111111111111111111111111111'

* A read request to the connection status characteristic returns one byte (byte 0) that reflects the status of current connection status and if connected also the SSID of the network it is connected to (bytes 1 - 19)

* Possible status codes are: 
    * 0x00 - Error connecting to the desired network
    * 0x01 - Not connected to any network
    * 0x02 - Connected
    * 0x03 - Attemtping to connect/working


#### select-network-characteristic.js ####

* uuid: '11111111111111111111111111111114'

* To select a network (user picks out his/hers network from the app after the available-networks-characteristic has supplied all the networks) the write request must contain the index of selected network, length of the password and password itself. 
*Let's look at the previous example and say the user selected network "WIFI2". Let's say the password for this network is "PASS". To select this network the app should write a buffer that looks like this: 
    * write = [0x02, 0x04, 0x50, 0x41, 0x53, 0x53]
* For longer passwords that do not fit in the 20 bytes supported by BLE the central must send the password in multiple packets. The read request characteristic waits until all the password bytes are received (second byte in the array, look at example) and then calls the selectNetwork() on the ConnectionManager 


#### wlan-addr-characteristic.js ####

* uuid: '11111111111111111111111111111113', 

* Reading from this characteristic returns the MAC address of the wlan0 interface - a 17 character string like "b8:27:eb:42:f1:44"


### TODO ###

* Error handling 
* Undefined conditions
* Sync with the app (what if some packets are lost...)
* Connection status response does not support payloads longer than 20 bytes (if status = Connected and attempts to send the SSID)
* Implement connecting to networks.